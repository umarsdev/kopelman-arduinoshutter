written by crstock in May 2017
provide to the Kopelman lab
primary contact in the lab is Jeff Folz
INC1054304

this project has two components:

1) An arduino program that takes a serial input. This gets commands a-j corresponding to positions in the servo range
2) An update to the lab's existing script.

there is a known issue where the setup portion of the serial port code for the Arduino shutter is in the main script, and probably should not be, but I can't identify the correct place for this code, and I can't identify where the other serial port is being setup. 
