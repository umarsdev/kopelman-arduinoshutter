//take commands a to z
//each serial command corresponds to a position in degrees from 0 to 180

#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int ledPin = 13;
int servoPin = 9;

//int ledPin = 9;
//int servoPin = 13;

int val; //store the servo output

void setup() {
  Serial.begin(9600);
  myservo.attach(servoPin);  // attaches the servo on pin 9 to the servo object
  pinMode(ledPin, OUTPUT);
}

void loop() {

}

void serialEvent(){
  while (Serial.available()){
    char inChar = (char)Serial.read();
    val = map(inChar, 'a', 'z', 0, 180); //map a to z = 0 to 180 degrees
    myservo.write(val);
   //Serial.println(val);
  }
  //provide a little feedback for testing
  if(val > 90){
    digitalWrite(ledPin, HIGH);
  }else{
    digitalWrite(ledPin, LOW);
  }
  
}

